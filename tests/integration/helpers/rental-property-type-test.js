
import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('rental-property-type', 'helper:rental-property-type', {
  integration: true
});

test('it shows Standalone if not listed on Community types', function(assert) {
  this.set('type', 'not listed');
  this.render(hbs`{{rental-property-type type}}`);

  assert.equal(this.$().text().trim(), 'Standalone', 'is not listed on Community types');
});

test('it shows Community if listed on Community types', function(assert) {
  this.set('type', 'Condo');
  this.render(hbs`{{rental-property-type type}}`);
  
  assert.equal(this.$().text().trim(), 'Community', 'is listed on Community types');
});

