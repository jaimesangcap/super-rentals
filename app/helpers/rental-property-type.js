import Ember from 'ember';

export const communityPropertyTypes = [
  'Condo',
  'Townhouse',
  'Apartment'
]

export function rentalPropertyType([propertyType]) {
  return communityPropertyTypes.includes(propertyType)
    ? 'Community'
    : 'Standalone'
}

export default Ember.Helper.helper(rentalPropertyType);
